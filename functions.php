<?php 
/* ############################################# */
// Añade herencia de estilos de tema padre
add_action( 'wp_enqueue_scripts', 'mi_tienda_online_enqueue_styles' );
function mi_tienda_online_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
} 

/* ############################################# */
// Personaliza contenido plantilla de página Home
add_action( 'init', 'convert_home_template_page_to_blank_page' );
function convert_home_template_page_to_blank_page(){

	remove_action( 'homepage', 'storefront_product_categories', 20 );
	remove_action( 'homepage', 'storefront_recent_products', 30 );
	remove_action( 'homepage', 'storefront_featured_products', 40 );
	remove_action( 'homepage', 'storefront_popular_products', 50 );
	remove_action( 'homepage', 'storefront_on_sale_products', 60 );
	remove_action( 'homepage', 'storefront_best_selling_products', 70 );
	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	remove_action( 'storefront_homepage', 'storefront_homepage_header', 10 );

	if ( is_front_page() ) {
		remove_action( 'storefront_content_top', 'woocommerce_breadcrumb', 10 );
	}
}

add_action( 'wp_enqueue_scripts', 'customize_blank_page_template' );
function customize_blank_page_template(){

	if ( is_page_template( 'template-blank.php' ) ) {

		remove_action( 'storefront_header', 'storefront_skip_links', 0 );
		remove_action( 'storefront_header', 'storefront_social_icons', 10 );
		remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
		remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42 );
		remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
		remove_action( 'storefront_header', 'storefront_header_cart', 60 );
		remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );
		remove_action( 'storefront_content_top', 'woocommerce_breadcrumb', 10 );
		remove_action( 'storefront_page', 'storefront_page_header', 10 );
	}
}

/* ##################################################### */
// [Cart Notice for WooCommerce] Elimina la etiqueda de (sin IVA) cuando se muestra la cantidad restante para alcanzar el precio mínimo en el carrito.
if ( class_exists( 'WooCommerce' ) && class_exists( 'BeRocket_cart_notices' ) ) {
	
	add_filter( 'wp_footer', 'delete_tax_label' );
	function delete_tax_label(){
		
		if ( is_cart() ) {
			?>
			<!-- Start BeRocket_cart_notices -->
			<script type="text/javascript">
			jQuery( document ).ready( function ($) {

				function search_and_delete_price_tax_label( ){
					var wc_notices = $( '.woocommerce-info' );

					wc_notices.each( function( i, e ){
						var tax_label = $( e ).find( '.woocommerce-Price-taxLabel' );

						if ( tax_label.length > 0 ) {
		
							tax_label.remove();
						};	
					});
				}

				search_and_delete_price_tax_label();

				// Elimina etiqueta del precio cuando se actualiza el carrito
				$( '.button[name="update_cart"]' ).click( function(){

					$( document ).ajaxSuccess( function(){
						setTimeout( search_and_delete_price_tax_label, 100 );
					});
				});
			});
			</script>
			<!-- End BeRocket_cart_notices -->
			<?php
		}
	}
}

?>