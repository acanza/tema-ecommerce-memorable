<?php
/**
 * The clean template.
 *
 *
 * Template name: Blank Page
 *
 * @package storefront
 */

get_header( 'blank' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();

				get_template_part( 'content', 'page' );

			endwhile; // End of the loop. ?>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer( 'blank' );
